DIR := $(shell dirname $(shell pwd)/$(lastword $(MAKEFILE_LIST)))
#uiauttag:= uiautomator
ROBOT_TAG:= environment_robot
APPIUM_TAG:= environment_appium

.PHONY: build	
build:
	docker-compose -f environment/execute_robot.yaml build 
#	docker-compose -f environment/uiautomation.yaml build 

#uiautomationviewer:
#	docker run -it --rm -e DISPLAY=$$DISPLAY \
	-v /tmp/.X11-unix:/tmp/.X11-unix  \
	--device /dev/bus/usb \
	${uiauttag} /home/AndroidSDK/tools/bin/uiautomatorviewer

test:
	robot -d test_root/Results test_root/Tests/shazam.robot
#	run_scripts/robot.sh shazam

#uiautomation:
#	docker run -it --rm -e DISPLAY=$$DISPLAY \
	-v /tmp/.X11-unix:/tmp/.X11-unix  \
	-v /dev/bus/usb:/dev/bus/usb \
	${uiauttag} uiautomatorviewer

#build_uiautomator:
#	docker build -f environment/uiautomator/Dockerfile -t ${uiauttag} environment/uiautomator/

#ui:
#	docker-compose -f environment/uiautomation.yaml up

docks:
	docker run -it --rm  \
	-v ${DIR}/test_root:/test \
	${ROBOT_TAG} /bin/bash -c "Scripts/tidy.sh && Scripts/doc_gen.sh"

appium:
	docker run -it --rm  \
	-v ${DIR}/test_root:/test \
    --device /dev/bus/usb \
    -p 4723:4723 \
	${APPIUM_TAG}  \
    /bin/bash -c "adb -a -P 5037 server nodeamon && appium"