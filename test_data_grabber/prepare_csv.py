import json
import os
import csv

dir_path = os.path.dirname(os.path.realpath(__file__))
names_list = []
with open("songs.json", "r") as file:
    names_list = json.load(file)


songs = os.listdir("mp3")

with open('songs.csv', 'w', newline='') as csvfile:
    fieldnames = ['title', 'subtitle' ,"name"]
    writer = csv.DictWriter(csvfile, fieldnames=fieldnames)

    for song in songs:
        song_dict = next(item for item in names_list if item["name"] == song.split(".")[0])
        song_dict.pop("href")
        song_dict["name"] = song_dict["name"] + ".mp3"

        writer.writerow(song_dict)