import json
import os

dir_path = os.path.dirname(os.path.realpath(__file__))
names_list = []
with open("songs.json", "r") as file:
    names_list = json.load(file)


songs = os.listdir("mp3")

for song in songs:
    song_dict = next(item for item in names_list if item["name"] == song.split(".")[0])
    print("playing " + song_dict["title"] + ": " + song_dict["subtitle"])
    os.system("mpg123 " + os.path.join(dir_path, song))