import os
import subprocess
import json

dir_path = os.path.dirname(os.path.realpath(__file__))

in_path = os.path.join(dir_path, "downloads")
out_path = os.path.join(dir_path, "mp3")

names_list = []
with open("songs.json", "r") as file:
    names_list = json.load(file)

for song_dict in names_list:
    in_ = os.path.join(in_path, song_dict["name"] + ".webm")
    out_ = os.path.join(out_path, song_dict["name"] + ".mp3")
    command = ["ffmpeg", "-i", in_, "-ss", "00:02:30", "-to", "00:02:50", out_]
    process = subprocess.Popen(command, stdout=subprocess.PIPE)
    process.wait()
    print(process.returncode)
