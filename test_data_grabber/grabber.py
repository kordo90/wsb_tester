# import webdriver
from selenium import webdriver
import json

# create webdriver object
driver = webdriver.Firefox()

NAME_SELECTOR = "//p/strong"

# url to playlist
url = "https://www.billboard.com/articles/news/list/9494940/best-songs-2020-top-100/"

# get
driver.get(url)

# get names
elements = driver.find_elements_by_xpath(NAME_SELECTOR)

names_list = []
for elem in elements:
    yt_link = None
    try:
        yt_link = elem.find_element_by_xpath("./a")
    except:
        pass
    if yt_link is not None:
        song_dict = {"href": yt_link.get_attribute("href")}
        text = elem.text.split(".", 1)[1].strip().split(",", 1)
        song_dict["title"] = text[0]
        song_dict["subtitle"] = text[1].strip().replace('"', "")
        file_name = song_dict["title"] + "_" + song_dict["subtitle"]
        song_dict["name"] = file_name.replace(" ", "_").replace("&", "and")
        names_list.append(song_dict)

with open("songs.json", "w") as file:
    json.dump(names_list, file)
