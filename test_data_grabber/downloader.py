from pytube import YouTube
import json

names_list = []

with open("songs.json", "r") as file:
    names_list = json.load(file)

for song_dict in names_list:
    try:
        yt = YouTube(song_dict["href"])
        video = yt.streams.filter(only_audio=True).desc().first()

    except:
        print("Exception on {}".format(song_dict["name"]))
        continue
    try:
        video.download("downloads", filename=song_dict["name"])
        print(song_dict["name"] + " - has been downloaded !!!")
    except:
        print("something went worng")
