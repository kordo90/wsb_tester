# !/bin/bash
robot_file=$1
in_docker=environment/execute_robot.yaml
out_docker=environment/.execute_robot.yaml
cat ${in_docker} | sed s/{NAME}/${robot_file}/g >> ${out_docker} 
docker-compose -f ${out_docker} up --abort-on-container-exit
rm ${out_docker}
