# P.Kordowski project for studies at WSB

## Technologies used:

* docker and docker-compose
* appium
* robot framework
* data driven testing
* sellenium (as data scraping tool)
* python/bash/Makefile (scripting) 

## What it can do?

Briefly it can play songs on host and test Shazam mobile app on device to find them.

## Details:

* adb and appium running in docker container
* robot running on host machine (It can ran in container as well but not playing songs)
* uses data driven library to get data from prepared csv file

## Difficulties

* Sahzam app has dynamic content especially tag button. Appium library works not well in such as situation.

### pass for movie and results
8RY6iGuHlgaYVtkFIQXy