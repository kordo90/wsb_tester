*** Settings ***
Documentation     **appium library wrappers**
...               This keywords are made to be used over appium framework
Library           AppiumLibrary

*** Keywords ***
Open Test Application
    [Documentation]    Wrapper over *Open Application*
    ...    All parameters are passed by globaly named variables
    Open Application    ${APPIUM_URL}    automationName=${ANDROID_AUTOMATION_NAME}
    ...    platformName=${ANDROID_PLATFORM_NAME}    platformVersion=${ANDROID_PLATFORM_VERSION}
    ...    app=${ANDROID_APP}    appPackage=${APP_PACKAGE}    appActivity=${APP_ACTIVITY}
