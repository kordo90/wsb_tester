*** Settings ***
Documentation     **main page of Shazam app keywords**
Library           AppiumLibrary
Variables         locators.yaml

*** Variables ***

*** Keywords ***
Click Tag button
    [Documentation]    Waits for and Clicks tag button (Shazam will be listen to the music)
    Wait Until Page Contains Element    ${TAG_BUTTON}
    #Click A Point    x=182    y=443    duration=100
    Click Element    ${TAG_BUTTON}

Wait For Listening
    [Documentation]    Waits until Shazam will be listening to the music
    Wait Until Page Does Not Contain    ${TAG_BUTTON}
    #Sleep    1
    ${result} =    Run Keyword And Return Status    Page Should Contain Element    ${ANDROID_ALLOW_BUTTON}
    #Run Keyword If    Page Should Contain Element    ${ANDROID_ALLOW_BUTTON}
    Run Keyword If    ${result}    Wait Until Page Contains Element    ${ANDROID_ALLOW_BUTTON}
    Run Keyword If    ${result}    Click Element    ${ANDROID_ALLOW_BUTTON}
    #Page Should Contain Element    ${TAGGING_TEXT}

Not Match View Has Been Displayed
    [Documentation]    Check if Shazam did not found a song
    Wait Until Page Contains Element    ${NO_MATCH_POPUP}
    Go Back

Match View Has Been Displayed
    [Documentation]    Check if Shazam found a song
    Wait Until Page Contains Element    ${MATCH_TITLE}
    ${title}=    Get Text    ${MATCH_TITLE}
    ${subtitle}=    Get Text    ${MATCH_SUBTITLE}
    Capture Page Screenshot    ${subtitle}_${title}.png
    Go Back
    [Return]    ${subtitle}    ${title}

Safe Back
    [Documentation]    Click back but ensure this is not main page
    ${result} =    Run Keyword And Return Status    Page Should Not Contain Element    ${TAG_BUTTON}
    Run Keyword If    ${result}    Go Back
