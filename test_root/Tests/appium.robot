*** Settings ***
Documentation     Simple example using AppiumLibrary
Library           AppiumLibrary
Variables         ../Variables.yaml

*** Variables ***
${ANDROID_APP}    ${APPS_PATH}/ApiDemos-debug.apk
${APP_PACKAGE}    io.appium.android.apis
${APP_ACTIVITY}    .app.SearchInvoke

*** Test Cases ***
Should send keys to search box and then check the value
    Open Test Application
    Input Search Query    Hello World!
    Submit Search
    Search Query Should Be Matching    Hello World!

*** Keywords ***
Input Search Query
    [Arguments]    ${query}
    Input Text    txt_query_prefill    ${query}

Submit Search
    Click Element    btn_start_search

Search Query Should Be Matching
    [Arguments]    ${text}
    Wait Until Page Contains Element    android:id/search_src_text
    Element Text Should Be    android:id/search_src_text    ${text}
