*** Settings ***
Documentation     Demonstration Shazam testing in Robot Framework
Library           DataDriver
Library           AppiumLibrary
Library           Process
Resource          ../Resources/appium_wrappers.robot
Variables         ../Variables.yaml
Resource          ../Resources/PageObjects/Shazam/pages.robot
Suite Setup       Open Test Application
Test Template     Valid Song Search
Test Teardown     Stop Song

*** Variables ***
${ANDROID_APP}    ${APPS_PATH}/shazam_v.11.14.0-210212.apk
${APP_PACKAGE}    com.shazam.android
${APP_ACTIVITY}    com.shazam.android.activities.SplashActivity

*** Test Cases ***
Search Song ${author} : ${title}
    def_author    def_title    def_filename

*** Keywords ***
Valid Song Search
    [Arguments]    ${author}    ${title}    ${filename}
    Start Song    ${mp3_dir}/${filename}
    Click Tag button
    Sleep    3
    ${tagged_author}    ${tagged_title}=    Match View Has Been Displayed
    Should Be Equal As Strings    ${tagged_author}    ${author}    ignore_case=True
    Should Be Equal As Strings    ${tagged_title}    ${title}    ignore_case=True

Start Song
    [Arguments]    ${song_path}
    Start Process    nvlc    -R    ${song_path}    alias=sound_play

Stop Song
    Run Keyword If Test Failed    Safe Back
    Terminate Process    sound_play
