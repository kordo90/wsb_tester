#!/usr/bin/env bash
#get script location
SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )"
files=()
files+=($(ls ${SCRIPT_DIR}/../Resources/*.robot))
for FILE_NAME in ${files[@]}
do 
    filename=$(basename -- "$FILE_NAME")
    filename="${filename%.*}"
    python3 -m robot.libdoc $FILE_NAME ${SCRIPT_DIR}/../Docs/${filename}.html
done
exit