#!/usr/bin/env bash
#get script location
SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )"
python3 -m robot.tidy --inplace \
${SCRIPT_DIR}/../Tests/*.robot \
${SCRIPT_DIR}/../Resources/*.robot \
${SCRIPT_DIR}/../Resources/PageObjects/Shazam/*.robot 
